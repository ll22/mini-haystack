# system modules
import memcache
import pymongo
import os
import logging
import sys
import socket
import struct
import time
import random
from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
from SocketServer import ThreadingMixIn
import threading
import httplib, urllib
from threading import Thread
import requests
import json
from fileIO import fileIO

# user defined modules
from fileIO import fileIO

# Global variables

CACHE_IP = 'localhost'
CACHE_PORT = 33333

# store is changing with mid
STORE_PORT = '5000'
STORE_IP = '172.17.0.'
# STORE_ADDR = STORE_IP + ':' + STORE_PORT

TEST_PHOTO_PATH = './test_photos/'


DELIMITER = '*'

ACTION_READ = 0
ACTION_WRITE = 1
ACTION_DELETE = 2
ACTION_READONLY = 10
ACTION_WRITEENABLE = 11

# set debug level
def setLogging(argv):
    if len(sys.argv) is 1:
        debugLevel = logging.INFO
    elif sys.argv[1] == 'debug':
        debugLevel = logging.DEBUG
    else:
        debugLevel = logging.INFO

    logging.basicConfig(level=debugLevel, format='%(message)s')

def debugMemcached():
    mc = memcache.Client(['127.0.0.1:11211'], debug=0)

    mc.set("some_key", "Some value")
    value = mc.get("some_key")
    print value

    mc.set("another_key", 3)
    mc.delete("another_key")

    mc.set("key", "1")   # note that the key used for incr/decr must be a string.
    print mc.get("key")
    mc.incr("key")
    print mc.get("key")
    mc.decr("key")
    print mc.get("key")

def writePhoto(photoID, photoData, LV):
    logging.debug('Cache Write! photoID: ' + str(photoID) + ' logicalVolum: ' + str(LV) )
    mc.set( str( photoID ) + ':' + str( LV ), photoData )
    return True # success

def readPhoto(destURL, Mid, LV, photoID):

    #print 'request photo infoList: ' + infoList
    logging.debug('Cache Read! photoID: ' + str(photoID) + ' logicalVolum: ' + str(LV) )
    #lookup Memcached
    data = mc.get( str( photoID ) + ':' + str( LV ) )
    #lookup Store
    if not data:
        print 'Read From Store'
        conn = httplib.HTTPConnection(destURL)
        infoList = str(ACTION_READ) + DELIMITER + str(photoID) + DELIMITER + str(LV)
        conn.request("GET", "/"+infoList)
        # print 'waiting to get ' + name
        r = conn.getresponse()
        assert r.status == 200
        data = r.read()
        conn.close()
        mc.set( str( photoID ) + ':' + str( LV ), data )
    else:
        print 'Read From Cache'
    return data

# def readFromLocalDir(fn):
#     f = open(fn, 'rb')
#     fileContent = f.read()
#     f.close()
#     return fileContent

# #single test for reading
# def testRequest(Mid, LV, photoID):
#     dataInfo = readPhoto(STORE_ADDR, Mid, LV, photoID)
#     if dataInfo[0] is False:
#        print 'Test \033[1;38mFail\033[1;m'
#     else:
#         data = dataInfo[1]
#         fn = str(photoID) + '.jpg'
#         refData = readFromLocalDir(TEST_PHOTO_PATH + fn)
#         if compareData(data, refData) is True:
#             print 'Test ' + fn + ' read back:' + '\033[1;32mPass\033[1;m'
#         else:
#             print 'Test ' + fn + ' read back:' + '\033[1;38mFail\033[1;m'

# def testUpload(LV, photoID, imageName):
#     photoData = readFromLocalDir(TEST_PHOTO_PATH + imageName)
#     return writePhoto(STORE_ADDR, photoID, photoData, LV) # (destURL, photoID, photoData, LV):


# def compareData(data1, data2):
#     if len(data1) != len(data2):
#         return False

#     byteNum = len(data1)

#     for i in range(0,  byteNum):
#         if data1[i] is not data2[i]:
#             return False
#     return True

# ################# Handler #################
class Handler(BaseHTTPRequestHandler):
    def do_GET(self):
        print 'GET received: ' + self.path
        self.send_response(200)
        self.end_headers()
        urlInfo = self.extractGetInfo(self.path) # get requested photo information
        Mid = int(urlInfo[0])
        LV = int(urlInfo[1])
        pid = int(urlInfo[2])
        STORE_ADDR = STORE_IP + str( 3 + Mid ) + ':' + STORE_PORT + str( Mid ) #create matched machine address
        print "****************", STORE_ADDR, Mid, LV, pid
        photoData = readPhoto(STORE_ADDR, Mid, LV, pid)
        self.wfile.write(photoData) # send photo data back to requester
        return

    def do_POST(self):
        print 'POST received: ' + self.path
        length = int(self.headers.getheader('content-length'))
        data = self.rfile.read(length)
        self.send_response(200)
        self.end_headers()

        urlInfo = self.extractPostInfo(self.path)
        photoID = int( urlInfo[0] )
        LV = int( urlInfo[1] )
        writePhoto(photoID, data, LV)
        return

    # return [photoID, LV]
    def extractPostInfo(self, requestPath): #  /12*1
        requestPath = requestPath.split('/')[1]
        urlInfo = requestPath.split(DELIMITER)
        logging.debug('[photoID, LV]: ' + str(urlInfo))
        pid = urlInfo[0]
        LV = urlInfo[1]
        return [pid, LV]

    # return [Mid, LV, photoID]
    def extractGetInfo(self, requestPath): #   /12*L1*P1
        requestPath = requestPath.split('/')[1]
        urlInfo = requestPath.split(DELIMITER)
        logging.debug('[Mid, LV, photoID]: ' + str(urlInfo))
        Mid = urlInfo[0]
        LV = urlInfo[1]
        pid = urlInfo[2]    

        return [Mid, LV, pid]

class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    """Handle requests in a separate thread."""

def main():
    try:
        server = ThreadedHTTPServer(("", CACHE_PORT), Handler)
        print 'Starting server, use <Ctrl-C> to stop'
        print 'Cache listening on port ' + str(CACHE_PORT) + '...'
        server.serve_forever()

    except KeyboardInterrupt:
        print '<Ctrl-C> received, shutting down the web server'
        server.socket.close()
        logging.debug('Cache server terminated')
    return

if  __name__ =='__main__':
    mc = memcache.Client(['127.0.0.1:11211'], debug=0)
    main()
