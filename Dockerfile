FROM ubuntu:14.04
RUN apt-get update && apt-get install -y python2.7
RUN apt-get install -y memcached
RUN sudo apt-get install -y mongodb-org
