

def test2(name):
    print 'Hello world! ' + name

def writeToFile(data, fileName, openOption): # openOption: 'a' or 'wb'
    phtoSize = len(data)

    fd = open(fileName, openOption)
    fd.write( data )
    offset = fd.tell() - phtoSize
    fd.close()
    #print 'offset is: ' + str(offset)
    #print 'size is: ' + str( phtoSize )
    return [offset, phtoSize]


def writeToFileMiddle(data, fileName, writeOffset):
    phtoSize = len(data)
    fd = open(fileName, 'w') 
    fd.seek(writeOffset) # move cursor to a specific position in the file
    fd.write( data )
    offset = fd.tell() - phtoSize
    assert offset == offset # initial writeOffset should equal to the calculate offset
    fd.close()
    return [offset, phtoSize]


def readFromFile(fn, offset, size):
    fd = open(fn, 'rb')
    fd.seek(offset) # go to the (offset+1)th byte in the file
    data = fd.read(size)
    fd.close()
    return data
