'''
Database:
1. LV -> MID: a hardcoeded dictioary LV2Mtable
'''
# system modules
import pymongo
import os
import logging
import sys
import socket
import struct
import time
import random
from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
from SocketServer import ThreadingMixIn
import threading
import httplib, urllib
from threading import Thread

# user defined modules
from fileIO import fileIO

# Global variables


STORE_IP = 'localhost' #172.17.0.(4+mid) 
STORE_PORT = 50001 #50000 + mid
machine = 1  #mid

DIREC_IP = '172.17.0.2'
DIREC_PORT = '22222'
DIREC_ADDR = DIREC_IP + ':' + DIREC_PORT
STORE_PORT_BASE = 50000

PV_PATH = '/store/physical_volumes'

needClean = False
garbageCollectorAlive = True
checkGarbagePeriod = 3 # seconds

try:
    connection = pymongo.Connection() # connect on the default host and port
except:
    os.system('service mongodb start')
    time.sleep(3)
    connection = pymongo.Connection() # try connect again

# db has several collections and each collection represents a table for one machine

db = connection['lookupTables'] # create database

# clean up mongoDB collections
db.Mtable.drop()
db.Mtable

DELIMITER = '*'
LV2Size = {} # a dictinary storing LV to its size
MEMLIM = 99999999999999999999999 # TODO: LV disk limitaion (MB)


ACTION_READ = 0
ACTION_WRITE = 1
ACTION_DELETE = 2
ACTION_READONLY = 10
ACTION_WRITEENABLE = 11

################ file I/O functions ##################
def generateRWpath(LV):
    global machine
    return PV_PATH + '/M' + str(machine) + '/' 'L' + str(LV)

# return [offset, size]
def savePhotoToMachine(LV, photoData):
    global machine
    # append photo data to the physical volume file
    #fn = PV_PATH + '/M' + str(machine) + '/' 'L' + str(LV)
    fn = generateRWpath(LV)
    logging.debug( 'file to save the photo data is: ' + fn )
    saveInfo = fileIO.writeToFile(photoData, fn, 'a')
    return [saveInfo[0], saveInfo[1]]


################ mongoDB functions ##################


def insert2Mtables(LV, photoID, flags, offset, size):
    print 'insert size: ', size
    print 'insert offset: ', offset

    db.Mtable.insert({"LV":LV, "ID":photoID, "flags":flags, "offset":offset, "size":size})

def lookupPhotoInfo(LV, pid):
    doc = db.Mtable.find_one({"LV":LV, "ID":pid })
    if doc is not None:
        return [ doc["flags"], doc["offset"], doc["size"] ] # [offset, size]
    logging.error('Lookup error: photo information not found!')
    return [-1, -1, -1] # error


################ photoRW functions ##################

def savePhoto(photoID, logicalVolum, photoData): # logicalVolum is int
    # save data to LV files in each machine
    saveInfo = savePhotoToMachine(logicalVolum, photoData)
    # Basing on which machine we uploaded photo to, update its <LV, pid> -> <offset, size> table
    insert2Mtables(logicalVolum, photoID, 'alive', saveInfo[0], saveInfo[1]) # args: Machines, LV, photoID, offset, size)
        

def readPhoto(LV, pid):
    photoInfo = lookupPhotoInfo(LV, pid)
    flags = photoInfo[0]
    offset = photoInfo[1]
    size = photoInfo[2]
    fn = generateRWpath(LV)
    if flags == 'deleted': #TODO Figure out the failure case
        return '404'
    data = fileIO.readFromFile(fn, offset, size )
    return data

def deletePhoto(PID, LV):
    global needClean
    #db.Mtable.find_and_modify(query={"LV":LV, "ID":PID }, update={"$set": {'flags': 'deleted'}}, upsert=False, full_response= True)
    #db.PID2LV.remove( { "PID" : PID } )
    db.Mtable.remove(  {"LV":LV, "ID":PID } )
    needClean = True 
    return 

# ################# ReadOnly/WriteEnable ####

def callDirectoryForReadOnly( LV ):
    conn = httplib.HTTPConnection(DIREC_ADDR)
    infoList = str( ACTION_READONLY ) + DELIMITER + str(LV)
    #print 'request photo infoList: ' + infoList
    try:
        conn.request("GET", "/"+infoList)
        # print 'waiting to get ' + name
        r = conn.getresponse()
        assert r.status == 200
        res = r.read().split(DELIMITER)
        conn.close()
    except:
        print 'request to request photo failed!'
        return False
    return True

def callDirectoryForWriteEnable( LV ):
    conn = httplib.HTTPConnection(DIREC_ADDR)
    infoList = str( ACTION_WRITEENABLE ) + DELIMITER + str(LV)
    #print 'request photo infoList: ' + infoList
    try:
        conn.request("GET", "/"+infoList)
        # print 'waiting to get ' + name
        r = conn.getresponse()
        assert r.status == 200
        res = r.read().split(DELIMITER)
        conn.close()
    except:
        print 'request to request photo failed!'
        return False
    return True



# ################# Handler #################
class Handler(BaseHTTPRequestHandler):
    def do_GET(self):
        print 'GET received: ' + self.path
        self.send_response(200)
        self.end_headers()
        urlInfo = self.extractGetInfo(self.path) # get requested photo information
        action = int(urlInfo[0])
        PID = int(urlInfo[1])
        LV = int(urlInfo[2])
        if action == ACTION_READ:
            photoData = readPhoto(LV, PID)
            self.wfile.write(photoData) # send photo data back to requester

        elif action == ACTION_DELETE:
            print 'Delete ' + str(PID) + ' ' + str(LV)
            deletePhoto(PID, LV)
            self.wfile.write('')
        return

    def do_POST(self):
        print 'POST received: ' + self.path
        length = int(self.headers.getheader('content-length'))
        data = self.rfile.read(length)
        self.send_response(200)
        self.end_headers()

        urlInfo = self.extractPostInfo(self.path)
        photoID = int( urlInfo[0] )
        LV = int( urlInfo[1] )
        savePhoto(photoID, LV, data)
        LV2Size[ LV ] += length
        if LV2Size[ LV ] >= MEMLIM:
            callDirectoryForReadOnly( LV )
        return

    # return [photoID, LV]
    def extractPostInfo(self, requestPath): #  /12*1
        requestPath = requestPath.split('/')[1]
        urlInfo = requestPath.split(DELIMITER)
        logging.debug('[photoID, LV]: ' + str(urlInfo))
        pid = urlInfo[0]
        LV = urlInfo[1]
        MIDs = urlInfo[2:]
        return [pid, LV, MIDs]

    # return [action, PID, LV, MID]
    def extractGetInfo(self, requestPath): #   /12*L1*P1
        requestPath = requestPath.split('/')[1]
        urlInfo = requestPath.split(DELIMITER)
        action = urlInfo[ 0 ]
        logging.debug('[action, PID, LV]: ' + str(urlInfo))
        PID = urlInfo[1]
        LV = urlInfo[2] 
        return [action, PID, LV]


class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    """Handle requests in a separate thread."""


def compactOneFile(LV, mapping):

    createdNewFile = False
    fn = generateRWpath(LV)
    for photoInfo in mapping:
        PID = photoInfo[0]
        offset = photoInfo[1]
        size = photoInfo[2]
        #print 'PID, offset, size: ', photoInfo
        #print 'fn is: ', fn
        data = fileIO.readFromFile(fn, offset, size ) # read out the photo data
        
        #fd = open(fn, 'rb')
        #fd.seek(offset) # go to the (offset+1)th byte in the file
        #data = fd.read(size)
        #fd.close()

        print 'len(data) is: ', len(data)

        # write the data to a new file
        newPhotoInfo = fileIO.writeToFile(data, fn+'_b', 'a') # args: data, fileName, openOption
        createdNewFile = True
        #db.Mtable.insert({"LV":LV, "ID":photoID, "flags":flags, "offset":offset, "size":size})
        # db.Mtable.remove(  {"LV":LV, "ID":PID } )
        db.Mtable.remove( {"LV":LV, "ID":PID } ) # delete the old entry in the lookup table
        db.Mtable.insert( {"LV":LV, "ID":PID, "flags":"alive", "offset":newPhotoInfo[0], "size":newPhotoInfo[1]} ) # new record

    if createdNewFile is True:
        rm_cmd = 'rm '+fn
        os.system(rm_cmd)
        print rm_cmd
        mv_cmd = 'mv '+fn+'_b' + ' ' + fn
        os.system(mv_cmd)
        print mv_cmd


def compactFiles():
    cursor = db.Mtable.find({})
    #currPhtoInfo = []
    lvList = {}

    #find the current memory mapping in all LV files
    for doc in cursor:
        print doc
        photoStatus = str(doc['flags'])
        #if photoStatus is not 'deleted':
            #print doc
        lv = doc['LV']
        PID = doc['ID']
        offset = doc['offset']
        size = doc['size']
        #currPhtoInfo.append([lv, offset, size])
        if lv not in lvList:
            lvList[lv] = []
        lvList[lv].append([PID, offset, size])

    print 'lvList', lvList
    for LV in lvList:
        compactOneFile( LV, lvList[LV] )


    #[[1, 0, 255787], [1, 255787, 8330], [1, 264117, 7540], [1, 271657, 12946], [1, 284603, 7024], [1, 291627, 8119], [1, 299746, 15697], [1, 315443, 9058], [1, 324501, 13990], [1, 338491, 6655]]
    #[[1, 264117, 7540], [1, 271657, 12946], [1, 284603, 7024], [1, 291627, 8119], [1, 315443, 9058], [1, 324501, 13990], [1, 338491, 6655]]

    #fn = generateRWpath(LV)
    #data = fileIO.readFromFile(fn, offset, size )


def garbageCollector():
    global needClean
    global garbageCollectorAlive
    global checkGarbagePeriod

    while garbageCollectorAlive is True:

        if needClean is False:
            print 'Garbage collector has no need to clean up'
        else:
            print 'cleaning logical volume files...'
            compactFiles()
            needClean = False

        time.sleep(checkGarbagePeriod)
    print 'Garbage collector finished'

def main():
    populateLV2Size(4)
    try:
        server = ThreadedHTTPServer(("", STORE_PORT), Handler)
        print 'Starting server, use <Ctrl-C> to stop'
        storeServerName = 'Store server ' + str( STORE_PORT - STORE_PORT_BASE ) 
        print storeServerName +' listening on port ' + str(STORE_PORT) + '...'
        t = Thread(target=garbageCollector, args=())
        t.setDaemon(True)
        t.start()
        time.sleep(1)
        server.serve_forever()

    except KeyboardInterrupt:
        print '<Ctrl-C> received, shutting down the web server'
        global garbageCollectorAlive
        garbageCollectorAlive = False
        server.socket.close()
        cleanUp()  # delete all saved physical volume files
        logging.debug('Store server terminated')


# set debug level
def setLogging(argv):
    if len(sys.argv) is 1:
        debugLevel = logging.INFO
    elif sys.argv[1] == 'debug':
        debugLevel = logging.DEBUG
    else:
        debugLevel = logging.INFO

    logging.basicConfig(level=debugLevel, format='%(message)s')

# delete saved physical volumes and delete lookup tables
def cleanUp():
    db.Mtable.drop()
    #### TODO rm error! maybe because of the lock?
    #### rm: cannot remove './physical_volumes/M1/*': No such file or directory
    cmd = 'rm -r ' + PV_PATH + '/*'  #'/M' + str(machine) + '/*'
    os.system(cmd)
    logging.debug(cmd)


# LV2Mtable -> LV -> M(achines)
def populateLV2Size(logicalVolumNum):
    # populate LV2Mtable 
    # eg: L1 -> M1 M2; L2 -> M2 M3; L3 -> M3 M4; L4 -> M4 M1
    # hardcoded 4 tables for now: todo: change it to dynamically create collections to represent M tables
    for i in range(1, logicalVolumNum+1):
        LV2Size[ i ] = 0
    ####TODO: if dir exits, do nothing. Since I change the function cleanup()
    cmd = 'mkdir ' + PV_PATH + '/M' + str(machine)
    os.system(cmd)

def init(argv):
    global machine
    global STORE_PORT
    if len( argv ) > 0:
        machine = int( argv[ 1 ] ) 
        STORE_PORT = STORE_PORT_BASE + machine

if  __name__ =='__main__':
    setLogging(sys.argv)
    init(sys.argv)
    main()
