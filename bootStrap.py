from threading import Thread
import os
import time
import sys

# \033[1;32mPass\033[1;m
GREEN_START = '\033[1;32m'
GREEN_END = '\033[1;m'
runServer = True

# start directory server
def startDirectory():
    startDirectoryDC = 'docker run -v /Users/lincongli/Desktop/2016_Fall/CSE291/proj_2/code/directory/:/directory -i  --rm --name=directory d31b651fa2c9 '
    print GREEN_START + 'Starting directory container...' + GREEN_END
    startApp = 'python directory/directoryServerHTTP.py &'
    cmd = startDirectoryDC + startApp
    print cmd
    os.system(cmd)
    #while True:
    #    time.sleep(1)

# start cache server
def startCache():
    startCacheDC = 'docker run -v /Users/lincongli/Desktop/2016_Fall/CSE291/proj_2/code/cache/:/cache -i  --rm --name=cache d31b651fa2c9 '
    startApp = 'python cache/cacheServerHTTP.py &'
    print GREEN_START + 'Starting cache container...' + GREEN_END
    cmd = startCacheDC + startApp
    print cmd
    os.system(cmd)
    #while True:
    #    time.sleep(1)

# start store servers
def startStore(i):
    startStoreDC = 'docker run --rm -v /Users/lincongli/Desktop/2016_Fall/CSE291/proj_2/code/store/:/store -i --name=store'+str(i)+' d31b651fa2c9 '
    startApp = 'python store/storeServerHTTP.py ' + str(i) + ' &'
    print GREEN_START + 'Starting store container ' +str(i) + '...' + GREEN_END
    cmd = startStoreDC + startApp
    print cmd
    os.system( cmd )
    #while True:
    #    time.sleep(1)


def startWebServerBash():
    # start web server bash
    startWebDC = 'docker run -v /Users/lincongli/Desktop/2016_Fall/CSE291/proj_2/code/testMultiThreadServer/:/testMultiThreadServer -t -i  --name=server --rm d31b651fa2c9 '
    startApp = '/bin/bash'
    #startApp = 'python testMultiThreadServer/pseudoServer.py'
    print GREEN_START + 'Starting webserver container...' + GREEN_END
    cmd = startWebDC + startApp
    print cmd
    os.system( cmd )
    #while True:
    #    time.sleep(1)


def startWebServer():
    global runServer
    # start web server
    startWebDC = 'docker run --rm -v /Users/lincongli/Desktop/2016_Fall/CSE291/proj_2/code/webServer/:/server -t -i  --name=server -p 33333:33333 -p 8001:8001 d31b651fa2c9 '
    if runServer is False:
        startApp = '/bin/bash'
    else:
        startApp = 'python server/webServer.py'
    print GREEN_START + 'Starting webserver container...' + GREEN_END
    cmd = startWebDC + startApp
    print cmd
    os.system( cmd )

if __name__ == '__main__':
    global runServer
    if len(sys.argv) is 2:
        runServer = False

    t1 = Thread(target=startDirectory, args=())
    #t1.setDaemon(True)
    t1.start()
    time.sleep(1)

    t2 = Thread(target=startCache, args=())
    #t2.setDaemon(True)
    t2.start()
    time.sleep(1)
    for i in range(1, 4+1):
        t3 = Thread(target=startStore, args=(i,))
        #t3.setDaemon(True)
        t3.start()
        time.sleep(1)

    startWebServer()

    # docker inspect directory | grep IPAddress
    # docker inspect cache | grep IPAddress
    # docker inspect store1 | grep IPAddress
    # docker inspect store2 | grep IPAddress
    # docker inspect store3 | grep IPAddress
    # docker inspect store4 | grep IPAddress

    # When web server container quits, stop all other running containers
    cmd = 'docker rm -f store1'
    os.system( cmd )

    cmd = 'docker rm -f store2'
    os.system( cmd )

    cmd = 'docker rm -f store3'
    os.system( cmd )

    cmd = 'docker rm -f store4'
    os.system( cmd )

    cmd = 'docker rm -f cache'
    os.system( cmd )

    cmd = 'docker rm -f directory'
    os.system( cmd )

    cmd = 'rm -rf ./store/physical_volumes/*'
    print 'OK! Everything stopped!!'

