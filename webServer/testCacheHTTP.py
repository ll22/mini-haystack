import httplib, urllib
from threading import Thread
import time
import requests
import json
import random
from fileIO import fileIO

CACHE_IP = '172.17.0.3'
CACHE_PORT = '33333'
CACHE_ADDR = CACHE_IP + ':' + CACHE_PORT

# store is changing with mid
STORE_PORT = '5000'
STORE_IP = '172.17.0.'


TEST_PHOTO_PATH = './test_photos/'

DELIMITER = '*'

ACTION_READ = 0
ACTION_WRITE = 1
ACTION_DELETE = 2
ACTION_READONLY = 10
ACTION_WRITEENABLE = 11

def readFromLocalDir(fn):
    f = open(fn, 'rb')
    fileContent = f.read()
    f.close()
    return fileContent


def compareData(data1, data2):
    if len(data1) != len(data2):
        return False

    byteNum = len(data1)

    for i in range(0,  byteNum):
        if data1[i] is not data2[i]:
            return False
    return True


def writePhoto(destURL, photoID, photoData, LV):
    url = 'http://' + destURL+'/' + str(photoID) + DELIMITER + str(LV)
    print 'write photo URL: ' + url
    payload = photoData
    r = requests.post(url, data=payload)
    try:
        assert r.status_code == 200
    except AssertionError:
        print 'write photo failed!'
        return False
    return True # success

def requestPhoto(destURL, Mid, LV, photoID):
    conn = httplib.HTTPConnection(CACHE_ADDR)
    infoList = str(Mid) + DELIMITER+ str(LV) + DELIMITER + str(photoID)
    #print 'request photo infoList: ' + infoList
    try:
        conn.request("GET", "/"+infoList)
        # print 'waiting to get ' + name
        r = conn.getresponse()
        assert r.status == 200
        data = r.read()
        conn.close()
    except:
        print 'read photo failed!'
        return [False, '']
    return [True, data]

#single test for reading
def testRequest(Mid, LV, photoID):
    dataInfo = requestPhoto(CACHE_ADDR, Mid, LV, photoID)
    if dataInfo[0] is False:
       print 'Test \033[1;38mFail\033[1;m'
    else:
        data = dataInfo[1]
        fn = str(photoID) + '.jpg'
        refData = readFromLocalDir(TEST_PHOTO_PATH + fn)
        if compareData(data, refData) is True:
            print 'Test ' + fn + ' read back:' + '\033[1;32mPass\033[1;m'
        else:
            print 'Test ' + fn + ' read back:' + '\033[1;38mFail\033[1;m'


def testUploadPhoto(LV, photoID, imageName):
    photoData = readFromLocalDir(TEST_PHOTO_PATH + imageName)
    ret = True
    ret |= writePhoto(CACHE_ADDR, photoID, photoData, LV) # (destURL, photoID, photoData, LV):
    Mid = LV
    STORE_ADDR = STORE_IP + str( 4 + Mid ) + ':' + STORE_PORT + str( Mid ) #create matched machine address
    ret |= writePhoto(STORE_ADDR, photoID, photoData, LV)
    Mid = LV + 1
    if Mid > 4:
        Mid = 1
    STORE_ADDR = STORE_IP + str( 4 + Mid ) + ':' + STORE_PORT + str( Mid ) #create matched machine address
    ret |= writePhoto(STORE_ADDR, photoID, photoData, LV)
    return ret



LV2Mtable = {} # a dictionary storing LV to Machine mapping (look up which machine)
# LV2Mtable -> LV -> M(achines)
def populateLV2Mtable(logicalVolumNum, replicationNum):
    # populate LV2Mtable 
    # eg: L1 -> M1 M2; L2 -> M2 M3; L3 -> M3 M4; L4 -> M4 M1
    for i in range(1, logicalVolumNum+1):
        LV2Mtable[i] = []
        for j in range(0, replicationNum):
            Mid = (i+j) % logicalVolumNum
            if Mid is 0:
                Mid = logicalVolumNum
            LV2Mtable[i].append( Mid  )


def randomPickLV(LVnum):
    return random.randint(1, LVnum)

def randomPickID(IDrange):
    return random.randint(1, IDrange)


def randomPickMid(Mids):
    randomIndex = random.randint(0, len(Mids) - 1)
    return Mids[randomIndex]

threadNum = 0

def requestManyPhoto(upLoadMap, uploadNum, requestNum):
    global threadNum
    for i in range(0, requestNum):
        randID = randomPickID(uploadNum)
        LV = upLoadMap[randID]
        Mids = LV2Mtable[LV]
        mid = randomPickMid(Mids)
        print str(i+1) + ' ',
        testRequest(mid, LV, randID) # testRequest(Mid, LV, photoID)

    threadNum -= 1

def test(uploadNum, requestNum, parallel, totalThread):
    global threadNum
    LVnum = 4
    replicaNum = 2
    populateLV2Mtable(LVnum, replicaNum)
    randomPickLV( LVnum )
    upLoadMap = {}
    # upload 
    for i in range(1, uploadNum+1):
        randLV = randomPickLV(LVnum)
        #time.sleep(0.1)
        upLoadMap[i] = randLV # ID -> LV
        if testUploadPhoto( randLV, i, str(i) + '.jpg') is True:
            print 'Uploaded ' + str(i) + '.jpg  LV is: ' + str(randLV)

    # request
    if parallel is False:
        requestManyPhoto(upLoadMap, uploadNum, requestNum)

    else:
        for i in range(0, totalThread):
            t = Thread(target=requestManyPhoto, args=(upLoadMap, uploadNum,requestNum,))
            threadNum += 1
            t.setDaemon(True)
            t.start()

        while threadNum > 0:
            time.sleep(1)

        print 'All threads finished!'

    '''
    testUploadPhoto(1, 12) # args: LV, ID
    time.sleep(0.5)
    testRequest(1, 1, 12) # args: Mid, LV, ID

    testUploadPhoto(1, 12) # args: LV, ID
    time.sleep(0.5)
    testRequest(1, 1, 12) # args: Mid, LV, ID   
    '''

def main():
    #parallel = False
    parallel = True
    totalThread = 4
    test(140, 500, parallel, totalThread)

    #testRequest()
    #testPost()
    #time.sleep(4)

if __name__ == '__main__':
    main()
