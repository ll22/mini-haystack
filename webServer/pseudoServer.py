import httplib, urllib
from threading import Thread
import time
import requests
import json
import random
from fileIO import fileIO


DIREC_IP = '172.17.0.2'
DIREC_PORT = '22222'
DIREC_ADDR = DIREC_IP + ':' + DIREC_PORT


CACHE_IP = '172.17.0.3'
CACHE_PORT = '33333'
CACHE_ADDR = CACHE_IP + ':' + CACHE_PORT

# store is changing with mid
STORE_PORT = '5000'
STORE_IP = '172.17.0.'


DELIMITER = '*'
TEST_PHOTO_PATH = '/server/test_photos/'


ACTION_READ = 0
ACTION_WRITE = 1
ACTION_DELETE = 2
ACTION_READONLY = 10
ACTION_WRITEENABLE = 11


############### Helper functions ################

def extractInfo(rawData):
    list = rawData.split(DELIMITER)
    ret = []
    for i in list:
        ret.append( int(i) )
    return ret


def readFromLocalDir(fn):
    f = open(fn, 'rb')
    fileContent = f.read()
    f.close()
    return fileContent


def compareData(data1, data2):
    if len(data1) != len(data2):
        return False

    byteNum = len(data1)

    for i in range(0,  byteNum):
        if data1[i] is not data2[i]:
            return False
    return True


def deleteFromStore(Mid, photoID, LV):
    STORE_ADDR = STORE_IP + str( 4 + Mid ) + ':' + STORE_PORT + str( 1+Mid ) #create matched machine address
    conn = httplib.HTTPConnection(STORE_ADDR)
    infoList = str(ACTION_DELETE) + DELIMITER + str(photoID) + DELIMITER + str(LV)
    conn.request("GET", "/"+infoList)
    # print 'waiting to get ' + name
    r = conn.getresponse()
    assert r.status == 200



def deleteFromDir(photoID):
    conn = httplib.HTTPConnection(DIREC_ADDR)
    infoList = str( ACTION_DELETE ) + DELIMITER + str(photoID)
    #print 'request photo infoList: ' + infoList
    #try:

    conn.request("GET", "/"+infoList)
    r = conn.getresponse()
    assert r.status == 200
    rawData = r.read() #int( r.read() )
    print rawData
    listInfo = extractInfo(rawData)
    LV = listInfo[0]
    MIDs = listInfo[1:]
    print 'To delete: ', photoID
    print 'LV: ', LV
    print 'MIDs', MIDs
    conn.close()

    #except:
    #print 'request to upload photo failed! T.T'
    #return [False, '']
    return [True, LV, MIDs]

# send PID to directory, and assigned by the directory with LV and MIDs
def UpdatePhotoToDir(photoID):
    conn = httplib.HTTPConnection(DIREC_ADDR)
    infoList = str( ACTION_WRITE ) + DELIMITER + str(photoID)
    #print 'request photo infoList: ' + infoList
    try:

        conn.request("GET", "/"+infoList)
        r = conn.getresponse()
        assert r.status == 200
        rawData = r.read() #int( r.read() )
        listInfo = extractInfo(rawData)
        LV = listInfo[0]
        MIDs = listInfo[1:]
        print 'To write: '
        print 'LV: ', LV
        print 'MIDs', MIDs
        conn.close()

    except:
        print 'request to upload photo failed! T.T'
        return [False, '']
    return [True, LV, MIDs]



def RequestPhotoInfoFromDir(photoID):
    conn = httplib.HTTPConnection(DIREC_ADDR)
    infoList = str( ACTION_READ ) + DELIMITER + str(photoID)
    #print 'request photo infoList: ' + infoList
    try:
        conn.request("GET", "/"+infoList)
        # print 'waiting to get ' + name
        r = conn.getresponse()
        assert r.status == 200
        res = r.read().split(DELIMITER)
        LV = int( res[ 0 ] )
        MID = int( res[ 1 ] )
        conn.close()
    except:
        print 'request to request photo failed!'
        return [False, '']
    return [True, LV, MID]



def randomPickID(IDrange):
    return random.randint(1, IDrange)



threadNum = 0
passNum = 0
testNum = 0

# request photo from cache. If not there, cacche would automatically request from store
def requestPhotoFromCacheStore(Mid, LV, photoID):
    conn = httplib.HTTPConnection(CACHE_ADDR)
    infoList = str(Mid) + DELIMITER+ str(LV) + DELIMITER + str(photoID)
    #print 'request photo infoList: ' + infoList
    try:
        conn.request("GET", "/"+infoList)
        # print 'waiting to get ' + name
        r = conn.getresponse()
        assert r.status == 200
        data = r.read()
        conn.close()
    except:
        print 'read photo failed!'
        return [False, '']
    return [True, data]


def requestOnePhoto(PID):
    # ask directory from which LV and machine to read the photo
    LV_MID = RequestPhotoInfoFromDir( PID )
    if LV_MID[ 0 ] is False:
        print 'Request photo from directory failed!'
        return [False]
    LV = LV_MID[1]
    if LV is -1: # photo deleted already!
        print '\033[1;32m Photo deleted. OK to fail >.> \033[1;m'
        return [False]

    MID = LV_MID[2]
    print "Request LV:", LV, " MachineID:", MID

    # ask cache for the photo
    dataInfo = requestPhotoFromCacheStore( MID, LV, PID)
    return dataInfo


# randomly request many photos based on the ones that have been uploaded
def requestManyPhoto(uploadNum, requestNum):
    global threadNum
    global passNum
    global testNum

    for i in range(0, requestNum): # repeat reading photo

        randPID = randomPickID(uploadNum)
        dataInfo = requestOnePhoto(randPID)

        if dataInfo[0] is False:
           print 'Test \033[1;31mFail\033[1;m  requestPhoto() return False!'
        else:
            data = dataInfo[1]
            fn = str(randPID) + '.jpg'
            refData = readFromLocalDir(TEST_PHOTO_PATH + fn)
            if compareData(data, refData) is True:
                print 'Test ' + fn + ' read back:' + '\033[1;32mPass\033[1;m'
                passNum += 1
            else:
                print 'Test ' + fn + ' read back:' + '\033[1;31mFail\033[1;m'

    testNum += requestNum

    threadNum -= 1


# write photo to EITHER cache or store depending on the URL
def writePhoto(destURL, photoID, photoData, LV):
    url = 'http://' + destURL+'/' + str(photoID) + DELIMITER + str(LV)
    print 'write photo URL: ' + url
    payload = photoData
    r = requests.post(url, data=payload)
    try:
        assert r.status_code == 200
    except AssertionError:
        print 'write photo failed!'
        return False
    return True # success

# write photo to BOTH cache and store
def writePhotoToCacheStore(photoID, LV , MIDs, photoData):
    # photoData = readFromLocalDir(TEST_PHOTO_PATH + imageName)
    ret = True
    # write to cache
    ret |= writePhoto(CACHE_ADDR, photoID, photoData, LV) # (destURL, photoID, photoData, LV):
    #Mid = LV
    # write photo to each store
    for Mid in MIDs:
        STORE_ADDR = STORE_IP + str( 4 + Mid ) + ':' + STORE_PORT + str( 1+Mid ) #create matched machine address
        ret |= writePhoto(STORE_ADDR, photoID, photoData, LV)
    return ret

# total test for server!!!!!!!!!!!!!!

def test(uploadNum, requestNum, parallel, totalThread):

    global threadNum
    global passNum
    global testNum
    # upload 
    for i in range(1, uploadNum+1):
        photoData = readFromLocalDir( TEST_PHOTO_PATH + str(i) + '.jpg' )
        PID = i # PID equals to the name of the photo for debug purpose
        LV_MIDs = UpdatePhotoToDir( PID ) 
        print LV_MIDs
        LV = LV_MIDs[ 1 ]
        MIDS = LV_MIDs[ 2 ]
        if LV_MIDs[ 0 ] is True:
            print "Upload LV: ", LV
            print "Upload MIDs: ", MIDS

        writePhotoToCacheStore(PID, LV , MIDS, photoData) # args: PID LV , MIDs, photoData

    # request
    if parallel is False:
        requestManyPhoto(uploadNum, requestNum)
    else:
        for i in range(0, totalThread):

            t = Thread(target=requestManyPhoto, args=(uploadNum,requestNum,))
            threadNum += 1
            t.setDaemon(True)
            t.start()

        while threadNum > 0:
            time.sleep(1)

        print 'All read threads finished!'
    
    print 'Correctness: ' + str( passNum*100.0 / testNum ) + '%'
    print 'About to start simulating delete...'
    time.sleep(5)
    # "uploadNum" number of photos have been uploaded
    # randomly pick some IDs from 1 to "uploadNum" to delete, and then test read those deleted photos
    deleteNum = 8
    deletePIDs = []
    for i in range(1, deleteNum+1):
        randID = random.randint(1, uploadNum)
        deletePIDs.append(randID) # allow repetitive deletion for now

    for deletePID in deletePIDs:
        # query directory for LV and MIDs
        deleteInfo = deleteFromDir(deletePID) # return [True, LV, MIDs]

        if deleteInfo[0] is False:
            print 'deleteFromDir returns false'
            print 'Stop deleting process'
        else:
            LV = deleteInfo[1]
            MIDs = deleteInfo[2]
            print 'LV: ', LV
            print 'MIDs: ', MIDs
            if LV is -1:
                print 'Photo already deleted!'
                continue
            else:
                for Mid in MIDs:
                    print 'Delete Blah'
                    deleteFromStore( Mid, deletePID, LV ) # deleteFromStore(Mid, photoID, LV)
            # for each MID in MIDs, send delete-request message to store to actually delete

    # test reading deleted photos, expected "error" messages
    # ....

    # test reading stored photos again, expecting to get them back correclty (to test file compacting)
    # request again!
    print 'About to read again to test file compacting...'
    time.sleep(5)

    if parallel is False:
        requestManyPhoto(uploadNum, requestNum)
    else:
        for i in range(0, totalThread):

            t = Thread(target=requestManyPhoto, args=(uploadNum,requestNum,))
            threadNum += 1
            t.setDaemon(True)
            t.start()

        while threadNum > 0:
            time.sleep(1)

        print 'All read threads finished!'    


def main():
    parallel = True
    totalThread = 2
    test(140, 50, parallel, totalThread)

if __name__ == '__main__':


    main()
