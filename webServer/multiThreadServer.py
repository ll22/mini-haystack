from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
from SocketServer import ThreadingMixIn
import threading
import os
import time

PORT = 33333
PHOTO_PATH = './uploaded_photos/'
PAGE_PHOTO_PATH = './page_photos/'
CSS_PATH = './css/'
pagePhotoList = ['HS.jpg', 'Haystack-upload.jpg', 'facebook-icons.jpg', 'haystack.jpg']


class Handler(BaseHTTPRequestHandler):

    #Handler for the GET requests. take care of photo read
    def do_GET(self):
        print 'HTTP request received is: ' + self.path

        try:
            #Check the file extension required and
            if self.path.endswith(".jpg"):
                path = self.path
                htmlName = path.split('/')[-1] # parse out image name
                self.serveData('image/jpg', htmlName)
                return 

            elif self.path.endswith(".html"):
                path = self.path
                htmlName = path.split('/')[-1] # parse out HTML file name
                self.serveData('text/html', htmlName)
                return
            elif self.path.endswith(".css"):
                 path = self.path
                 cssName = path.split('/')[-1] # parse out HTML file name
                 self.serveData('text/css', cssName)
                 return

            else:
                # should not reach here
                self.default_reply(self.path)


        except IOError:
            self.send_error(404,'File Not Found: %s' % self.path)


    def serveData(self, mimetype, fn): # request for server to send data
        # if found write back that file else write back 404
        if mimetype == 'text/html':
            if fn == 'index.html': # generate new index.html
                self.generateIndexHTML()
                f = open(fn, 'rb')
                fileContent = f.read()
                f.close()
        elif mimetype == 'image/jpg':
            #if 'facebook-icons.jpg' in fn or 'Haystack_serve.jpg' in fn:
            if fn in pagePhotoList:
                f = open(PAGE_PHOTO_PATH + fn, 'rb')
            else:
                # self.delayOnPurpose(fn, 3)
                f = open(PHOTO_PATH + fn, 'rb')
            fileContent = f.read()
            f.close()
        elif mimetype == 'text/css':
            f = open(CSS_PATH + fn, 'rb')
            self.send_response(200)
            self.send_header('Content-type', 'text/css')
            fileContent = f.read()
            f.close()
        else:
            print 'Error in serveData: Not implemented yet!'
            f = open(fn, 'rb')
            fileContent = f.read()
            f.close()

        self.send_response(200) # OK
        self.send_header('Content-type',mimetype)
        self.send_header('Cache-Control', 'no-store, must-revalidate, max-age=0')
        self.send_header('Pragma', 'no-cache')
        self.end_headers()
        self.wfile.write(fileContent) # send file content


    # generate index.html file base on uploaded photos in the "uoloaded_photos" directory
    def generateIndexHTML(self):
        fd1 = open('template_index.html', 'rb')
        fd2 = open('index.html', 'wb')

        for line in fd1:
            fd2.write(line)
            if '<!-- upload to here -->' in line:
                fileList = os.listdir(PHOTO_PATH) # search './uploaded_photos'
                # print fileList
                for eachFile in fileList:
                    if '.jpg' in eachFile:
                        line = '<div class="col-md-6 col3">\n<h3> ' + eachFile \
                        +  ' </h3>\n<button type="submit" class="btn btn-default btn-green">Delete</button>\n' \
                        +  '<button type="submit" class="btn btn-default btn-green" onclick="window.open(\'http://127.0.0.1:33333/' + eachFile + '\')">View</button>\n</div>\n\n'

                        # line = '<input type="button" value=\"' + eachFile + '\" onclick="window.open(\'http://127.0.0.1:33333/'+ eachFile + '\')" />\n'
                        fd2.write(line)

        fd1.close()
        fd2.close()


    def default_reply(self, request):
        self.send_response(200)
        self.send_header('Content-type','text/html')
        self.end_headers()
        # Send the html message
        msg = 'Request: ' + request + ' has not been implemented yet T.T'
        self.wfile.write(msg)

    def delayOnPurpose(self, fn, sec):
        print 'Delaying for ' + str(sec) + ' seconds... to server ' + fn
        time.sleep(sec)


    # take care of photo write
    def do_POST(self):
        print self.headers
        print 'POST received: ' + self.path
        length = int(self.headers.getheader('content-length'))
        data = self.rfile.read(length)
        print data
        self.send_response(200)
        self.end_headers()


class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    """Handle requests in a separate thread."""

def startPHPserver():
    os.system('php -S 127.0.0.1:8001 -t .') # start php server for uploading photo

def main():
    try:
        server = ThreadedHTTPServer(('', PORT), Handler)
        print 'Starting server, use <Ctrl-C> to stop'
        print "serving at port", PORT

        # run PHP server in another thread
        phpServerThread = threading.Thread(target=startPHPserver, args=())
        phpServerThread.setDaemon(True)
        phpServerThread.start()
        server.serve_forever()

    except KeyboardInterrupt:
        print '<Ctrl-C> received, shutting down the web server'
        server.socket.close()
        cmd = 'rm ./uploaded_photos/*.jpg'
        print cmd
        os.system(cmd)

if __name__ == '__main__':
    main()





