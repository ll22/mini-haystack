from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
from SocketServer import ThreadingMixIn
import threading
import os
import time
import requests
import json
import httplib, urllib

####### for printing with color
GREEN_START = '\033[1;32m'
GREEN_END = '\033[1;m'

RED_START = '\033[91m'
RED_END = '\033[0m'

####### system directories for page rendering
PORT = 33333

DIR_OFFSET = '/server/'
PHOTO_PATH = '/server/uploaded_photos/'
PAGE_PHOTO_PATH = '/server/page_photos/'
CSS_PATH = '/server/css/'
pagePhotoList = ['HS.jpg', 'Haystack-upload.jpg', 'facebook-icons.jpg', 'haystack.jpg', 'delete_failed.jpg', 'delete_succeed.jpg']


DIREC_IP = '172.17.0.2'
DIREC_PORT = '22222'
DIREC_ADDR = DIREC_IP + ':' + DIREC_PORT

CACHE_IP = '172.17.0.3'
CACHE_PORT = '33333'
CACHE_ADDR = CACHE_IP + ':' + CACHE_PORT

# store is changing with mid
STORE_PORT = '5000'
STORE_IP = '172.17.0.'

DELIMITER = '*'

ACTION_READ = 0
ACTION_WRITE = 1
ACTION_DELETE = 2
ACTION_READONLY = 10
ACTION_WRITEENABLE = 11


####### global variables
# photoName -> PID lookup table
directoryWatcherAlive = True
storedPhotoTable = {}
currPID = 1


############### Helper functions ################
def wrapGreen(text):
    return GREEN_START+text+GREEN_END

def wrapRed(text):
    return RED_START+text+RED_END


def extractInfo(rawData):
    list = rawData.split(DELIMITER)
    ret = []
    for i in list:
        ret.append( int(i) )
    return ret

############### Action functions ################


############### Delete ###################
def deleteFromStore(Mid, photoID, LV):
    STORE_ADDR = STORE_IP + str( 4 + Mid ) + ':' + STORE_PORT + str( 1+Mid ) #create matched machine address
    conn = httplib.HTTPConnection(STORE_ADDR)
    infoList = str(ACTION_DELETE) + DELIMITER + str(photoID) + DELIMITER + str(LV)
    conn.request("GET", "/"+infoList)
    # print 'waiting to get ' + name
    r = conn.getresponse()
    try:
        assert r.status == 200
    except:
        print 'Delete From Store Failed'
        return False
    return True

def deleteFromDir(photoID):
    conn = httplib.HTTPConnection(DIREC_ADDR)
    infoList = str( ACTION_DELETE ) + DELIMITER + str(photoID)
    #print 'request photo infoList: ' + infoList
    #try:

    conn.request("GET", "/"+infoList)
    r = conn.getresponse()
    assert r.status == 200
    rawData = r.read() #int( r.read() )
    print rawData
    listInfo = extractInfo(rawData)
    LV = listInfo[0]
    MIDs = listInfo[1:]
    print 'To delete: ', photoID
    print 'LV: ', LV
    print 'MIDs', MIDs
    conn.close()

    #except:
    #print 'request to upload photo failed! T.T'
    #return [False, '']
    return [True, LV, MIDs]


############### Upload ###################

# send PID to directory, and assigned by the directory with LV and MIDs
def UpdatePhotoToDir(photoID):
    conn = httplib.HTTPConnection(DIREC_ADDR)
    infoList = str( ACTION_WRITE ) + DELIMITER + str(photoID)
    #print 'request photo infoList: ' + infoList
    try:

        conn.request("GET", "/"+infoList)
        r = conn.getresponse()
        assert r.status == 200
        rawData = r.read() #int( r.read() )
        listInfo = extractInfo(rawData)
        LV = listInfo[0]
        MIDs = listInfo[1:]
        print 'To write: '
        print 'LV: ', LV
        print 'MIDs', MIDs
        conn.close()

    except:
        print 'request to upload photo failed! T.T'
        return [False, '']
    return [True, LV, MIDs]


# write photo to BOTH cache and store
def writePhotoToCacheStore(photoID, LV , MIDs, photoData):
    ret = True
    # write to cache
    ret |= writePhoto(CACHE_ADDR, photoID, photoData, LV) # (destURL, photoID, photoData, LV):
    #Mid = LV
    # write photo to each store
    for Mid in MIDs:
        STORE_ADDR = STORE_IP + str( 4 + Mid ) + ':' + STORE_PORT + str( 1+Mid ) #create matched machine address
        ret |= writePhoto(STORE_ADDR, photoID, photoData, LV)
    return ret



# write photo to EITHER cache or store depending on the URL
def writePhoto(destURL, photoID, photoData, LV):
    url = 'http://' + destURL+'/' + str(photoID) + DELIMITER + str(LV)
    print 'write photo URL: ' + url
    payload = photoData
    r = requests.post(url, data=payload)
    try:
        assert r.status_code == 200
    except AssertionError:
        print 'write photo failed!'
        return False
    return True # success


############### Request ###################
def requestOnePhoto(PID):
    # ask directory from which LV and machine to read the photo
    LV_MID = RequestPhotoInfoFromDir( PID )
    if LV_MID[ 0 ] is False:
        print wrapRed('Request photo from directory failed... *.*' )
        return [False]
    LV = LV_MID[1]
    if LV is -1: # photo deleted already!
        print wrapGreen('Photo deleted. OK to fail >.> ')
        return [False]

    MID = LV_MID[2]
    #print "Request LV:", LV, " MachineID:", MID

    # ask cache for the photo
    dataInfo = requestPhotoFromCacheStore( MID, LV, PID)
    return dataInfo


def RequestPhotoInfoFromDir(photoID):
    conn = httplib.HTTPConnection(DIREC_ADDR)
    infoList = str( ACTION_READ ) + DELIMITER + str(photoID)
    #print 'request photo infoList: ' + infoList
    try:
        conn.request("GET", "/"+infoList)
        # print 'waiting to get ' + name
        r = conn.getresponse()
        assert r.status == 200
        res = r.read().split(DELIMITER)
        LV = int( res[ 0 ] )
        MID = int( res[ 1 ] )
        conn.close()
    except:
        print 'request to request photo failed!'
        return [False, '']
    return [True, LV, MID]


# request photo from cache. If not there, cacche would automatically request from store
def requestPhotoFromCacheStore(Mid, LV, photoID):
    conn = httplib.HTTPConnection(CACHE_ADDR)
    infoList = str(Mid) + DELIMITER+ str(LV) + DELIMITER + str(photoID)
    #print 'request photo infoList: ' + infoList
    try:
        conn.request("GET", "/"+infoList)
        # print 'waiting to get ' + name
        r = conn.getresponse()
        assert r.status == 200
        data = r.read()
        conn.close()
    except:
        print wrapRed('read photo from store failed... *.*')
        return [False, '']
    return [True, data]



class Handler(BaseHTTPRequestHandler):

    #Handler for the GET requests. take care of photo read
    def do_GET(self):
        print 'HTTP request received is: ' + self.path
        if self.path is '/': # server index.html
            self.path += 'index.html'

        try:
            #Check the file extension required and
            if self.path.endswith(".jpg"):
                path = self.path
                htmlName = path.split('/')[-1] # parse out image name

                if 'delete' in path: # a delete request
                    print wrapGreen('Delete request: '+htmlName)
                    res = deletePhoto(htmlName)
                    if res is True: # delete succeed
                        self.serveData('image/jpg', 'delete_succeed.jpg')
                    else: 
                        self.serveData('image/jpg', 'delete_failed.jpg')
                    
                    return 

                else: # serve photo
                    self.serveData('image/jpg', htmlName)
                    return 

            elif self.path.endswith(".html"):
                path = self.path
                htmlName = path.split('/')[-1] # parse out HTML file name
                self.serveData('text/html', htmlName)
                return
            elif self.path.endswith(".css"):
                 path = self.path
                 cssName = path.split('/')[-1] # parse out HTML file name
                 self.serveData('text/css', cssName)
                 return

            else:
                # should not reach here
                self.default_reply(self.path)


        except IOError:
            self.send_error(404,'File Not Found: %s' % self.path)


    def serveData(self, mimetype, fn): # request for server to send data

        # print 'fn is' + fn
        # if found write back that file else write back 404
        if mimetype == 'text/html':
            print 'Got here!'
            if fn == 'index.html' or '': # generate new index.html
                self.generateIndexHTML()
                f = open(DIR_OFFSET + fn, 'rb')
                fileContent = f.read()
                f.close()

        elif mimetype == 'image/jpg':
            #if 'facebook-icons.jpg' in fn or 'Haystack_serve.jpg' in fn:
            if fn in pagePhotoList:
                f = open(PAGE_PHOTO_PATH + fn, 'rb')
                fileContent = f.read()
                f.close()

            else: # photo requested is user-uploaded photo

                # self.delayOnPurpose(fn, 3)
                print wrapGreen('Requesting ' + fn)

                if fn not in storedPhotoTable:
                    print wrapRed('Photo has been deleted... *.*')
                    f = open('/server/imageNotFound.jpg', 'rb') # render photo-not-found default page
                    fileContent = f.read()
                    f.close()

                else:
                    LV = storedPhotoTable[fn]
                    requestInfo = requestOnePhoto(LV)
                    if requestInfo[0] is False:
                       print wrapRed('Request photo failed... *.*')
                       f = open('/server/imageNotFound.jpg', 'rb') # render photo-not-found default page
                       fileContent = f.read()
                       f.close()

                    else:
                        print wrapGreen('Request photo succeed >.>')
                        fileContent = requestInfo[1]


        elif mimetype == 'text/css':
            f = open(CSS_PATH + fn, 'rb')
            self.send_response(200)
            self.send_header('Content-type', 'text/css')
            fileContent = f.read()
            f.close()
        else:
            print 'Error in serveData: Not implemented yet!'
            f = open(fn, 'rb')
            fileContent = f.read()
            f.close()

        self.send_response(200) # OK
        self.send_header('Content-type',mimetype)
        self.send_header('Cache-Control', 'no-store, must-revalidate, max-age=0')
        self.send_header('Pragma', 'no-cache')
        self.end_headers()
        self.wfile.write(fileContent) # send file content


    # generate index.html file base on uploaded photos in the "uoloaded_photos" directory
    def generateIndexHTML(self):
        fd1 = open(DIR_OFFSET + 'template_index.html', 'rb')
        fd2 = open(DIR_OFFSET + 'index.html', 'wb')

        for line in fd1:
            fd2.write(line)
            if '<!-- upload to here -->' in line:
                #fileList = os.listdir(PHOTO_PATH) # search '/server/uploaded_photos'
                photoList = storedPhotoTable.keys()
                # print photoList
                for eachPhoto in photoList:
                    deleteURL = 'http://192.168.99.100:33333/delete/' + eachPhoto
                    deleteURL = "\'" + deleteURL + "\'"

                    if '.jpg' in eachPhoto:
                        line = '<div class="col-md-6 col3">\n<h3> ' + eachPhoto \
                        +  ' </h3>\n<button type="submit" class="btn btn-default btn-green" onclick=\"location.href =  ' + deleteURL + '\" >Delete</button>\n' \
                        +  '<button type="submit" class="btn btn-default btn-green" onclick="window.open(\'http://192.168.99.100:33333/' + eachPhoto + '\')">View</button>\n</div>\n\n'

                        # <a onclick="location.href = 'http://192.168.99.100:8001/';" id="UPload">
                        # <button type="submit" class="btn btn-default btn-green" onclick="location.href = 'http://192.168.99.100:33333/delete/137.jpg'">Delete</button>
                        # line = '<input type="button" value=\"' + eachPhoto + '\" onclick="window.open(\'http://127.0.0.1:33333/'+ eachPhoto + '\')" />\n'
                        fd2.write(line)

        fd1.close()
        fd2.close()


    def default_reply(self, request):
        self.send_response(200)
        self.send_header('Content-type','text/html')
        self.end_headers()
        # Send the html message
        msg = 'Request: ' + request + ' has not been implemented yet T.T'
        self.wfile.write(msg)

    def delayOnPurpose(self, fn, sec):
        print 'Delaying for ' + str(sec) + ' seconds... to server ' + fn
        time.sleep(sec)


    # take care of photo write
    def do_POST(self):
        print self.headers
        print 'POST received: ' + self.path
        length = int(self.headers.getheader('content-length'))
        data = self.rfile.read(length)
        print data
        self.send_response(200)
        self.end_headers()


class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    """Handle requests in a separate thread."""

def startPHPserver():
    # os.system('php -S 127.0.0.1:8001 -t .') # start php server for uploading photo
    os.system('php -S 0.0.0.0:8001 -t /server/') # when it's running in Docker, address should be "0.0.0.0"


def deletePhoto(photoName):
    global storedPhotoTable

    # query directory for LV and MIDs
    if photoName not in storedPhotoTable:
        print wrapRed(photoName + ' has already been deleted! Not deleting again')
        return 

    deletePID = storedPhotoTable[photoName] # look up PID

    deleteInfo = deleteFromDir(deletePID) # return [True, LV, MIDs]

    if deleteInfo[0] is False:
        print 'deleteFromDir returns false'
        print 'Stop deleting process'
        return  
    else:
        LV = deleteInfo[1]
        MIDs = deleteInfo[2]
        #print 'LV: ', LV
        #print 'MIDs: ', MIDs
        if LV is -1:
            print wrapRed(photoName + ' has already been deleted! Not deleting again')

        else:
            for Mid in MIDs:
                res = deleteFromStore( Mid, deletePID, LV ) # deleteFromStore(Mid, photoID, LV)
                if res is True:
                    print wrapGreen('Delete succeed! >.>')
                else:
                    print wrapRed('Delete failed! *.*')
                    return False
            storedPhotoTable.pop(photoName)
            return True



def uploadPhoto(photoData, PID):
    # ask directory to assign Logical Volumes and machine IDs
    LV_MIDs = UpdatePhotoToDir(PID) # return [True, LV, MIDs]

    if LV_MIDs[ 0 ] is False:
        print wrapRed('Upload photo to directory failed... Upload stopped *.*')
        return # stop uploading

    else:

        print wrapGreen('Upload photo to directory succeed >.>')
        LV = LV_MIDs[ 1 ]
        MIDS = LV_MIDs[ 2 ]
        print "Upload LV: ", LV
        print "Upload MIDs: ", MIDS
        res = writePhotoToCacheStore(PID, LV , MIDS, photoData) # args: PID LV , MIDs, photoData
        if res is True:
            print wrapGreen('Upload photo to cache and stores succeed >.>')
        else:
            print wrapRed('Upload photo to cache and stores failed *.*')

# keep track of any photo that has been uploaded to this directory. Once find new photos, trigger upload photo action
def directoryWatcher():

    global directoryWatcherAlive
    photoUploadDir = '/server/uploaded_photos/'
    photoBackupDir = '/server/photo_backup/' # os.rename(
    while directoryWatcherAlive is True:
        photoList = os.listdir( photoUploadDir )
        global storedPhotoTable
        global currPID
        #print 'photoList: ', len(photoList) 
        for photoName in photoList:
            # assign a photo ID to that photo
            # make sure the photo name does not exist
            originalName = photoName
            if photoName in storedPhotoTable:
                photoNamePostFixNum = 1
                photoName += ('_' + str(photoNamePostFixNum))
                while photoName in storedPhotoTable:
                    photoNamePostFixNum += 1 # keep increasing photoNamePostFixNum until we composed a name that does not exist
                    photoName += ('_' + str(photoNamePostFixNum))
                os.rename(photoUploadDir + originalName, photoUploadDir + photoName)

            storedPhotoTable[photoName] = currPID
            currPID += 1

            # contact directory, stores and cache to actually store the photo
            PID = storedPhotoTable[photoName]

            f = open(photoUploadDir + photoName, 'rb')
            photoData = f.read()
            f.close()

            uploadPhoto(photoData, PID)

            print wrapGreen('In directoryWatcher: photoName uploaded to directory, stores and cache!')

            # remove photo right after it's uploaded
            #os.remove(photoUploadDir + originalName)
            os.rename(photoUploadDir + photoName, photoBackupDir + photoName)
            print photoName + ' removed from ' + photoUploadDir



def main():
    try:
        server = ThreadedHTTPServer(('', PORT), Handler)
        print wrapGreen('Starting server, use <Ctrl-C> to stop')
        print "serving at port", PORT

        # run PHP server in another thread
        phpServerThread = threading.Thread(target=startPHPserver, args=())
        phpServerThread.setDaemon(True)
        phpServerThread.start()

        # start directory watcher to be ready to upload photo whenever a photo is uploaded
        dirWactherThread = threading.Thread( target=directoryWatcher, args=() )
        dirWactherThread.setDaemon(True)
        dirWactherThread.start()

        server.serve_forever()

    except KeyboardInterrupt:
        # trying to shut down gracefully
        print wrapGreen('\n\n<Ctrl-C> received, trying to shut down the web server gracefully...')
        global directoryWatcherAlive
        directoryWatcherAlive = False # let directory watcher finish
        server.socket.close()

        # remove server local copies of photos
        cmd = 'rm /server/uploaded_photos/*.jpg'
        print  wrapGreen(cmd)
        os.system(cmd)
        cmd = 'rm /server/photo_backup/*'
        print wrapGreen(cmd)
        os.system(cmd)
        print wrapGreen('Server is down!')


if __name__ == '__main__':
    main()





