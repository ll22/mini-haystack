import httplib, urllib
from threading import Thread
import time
import requests
import json
import random
from fileIO import fileIO

ACTION_READ = 0
ACTION_WRITE = 1

DIREC_IP = '172.17.0.2'
DIREC_PORT = '22222'
DIREC_ADDR = DIREC_IP + ':' + DIREC_PORT

DELIMITER = '*'

def extractInfo(rawData):
    list = rawData.split(DELIMITER)
    ret = []
    for i in list:
        ret.append( int(i) )
    return ret

def testUploadPhoto(destURL, photoID):
    conn = httplib.HTTPConnection(DIREC_ADDR)
    infoList = str( ACTION_WRITE ) + DELIMITER + str(photoID)
    #print 'request photo infoList: ' + infoList
    try:

        conn.request("GET", "/"+infoList)
        r = conn.getresponse()
        assert r.status == 200
        rawData = r.read() #int( r.read() )
        listInfo = extractInfo(rawData)
        LV = listInfo[0]
        MIDs = listInfo[1:]
        print 'LV: ', LV
        print 'MIDs', MIDs
        conn.close()

    except:
        print 'request to upload photo failed! T.T'
        return [False, '']
    return [True, LV]

def testRequestPhoto(destURL, photoID):
    conn = httplib.HTTPConnection(DIREC_ADDR)
    infoList = str( ACTION_READ ) + DELIMITER + str(photoID)
    #print 'request photo infoList: ' + infoList
    try:
        conn.request("GET", "/"+infoList)
        # print 'waiting to get ' + name
        r = conn.getresponse()
        assert r.status == 200
        res = r.read().split(DELIMITER)
        LV = int( res[ 0 ] )
        MID = int( res[ 1 ] )
        conn.close()
    except:
        print 'request to request photo failed!'
        return [False, '']
    return [True, LV, MID]

def randomPickID(IDrange):
    return random.randint(1, IDrange)

threadNum = 0

def requestManyPhoto(uploadNum, requestNum):
    global threadNum
    for i in range(0, requestNum):
        randID = randomPickID(uploadNum)
        res = testRequestPhoto( DIREC_ADDR, randID )
        if res[ 0 ]:
            print "Request LV:", res[ 1 ], " MID:", res[ 2 ] 
    threadNum -= 1


# total test!!!

def test(uploadNum, requestNum, parallel, totalThread):
    global threadNum
    # upload 
    for i in range(1, uploadNum+1):
        res = testUploadPhoto( DIREC_ADDR, i ) 
        if res[ 0 ] is True:
            print "Upload LV:", res[ 1 ]

    # request
    if parallel is False:
        requestManyPhoto(uploadNum, requestNum)
    else:
        for i in range(0, totalThread):

            t = Thread(target=requestManyPhoto, args=(uploadNum,requestNum,))
            threadNum += 1
            t.setDaemon(True)
            t.start()

        while threadNum > 0:
            time.sleep(1)

        print 'All threads finished!'


def main():
    parallel = False
    totalThread = 4
    test(100, 200, parallel, totalThread)

    #testRequest()
    #testPost()
    #time.sleep(4)

if __name__ == '__main__':
    main()
