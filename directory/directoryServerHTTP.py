# system modules
import pymongo
import os
import logging
import sys
import socket
import struct
import time
import random
from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
from SocketServer import ThreadingMixIn
import threading

ACTION_READ = 0
ACTION_WRITE = 1
ACTION_DELETE = 2
ACTION_READONLY = 10
ACTION_WRITEENABLE = 11

# Global variables
DIREC_IP = 'localhost'
DIREC_PORT = 22222

try:
    connection = pymongo.Connection() # connect on the default host and port
except:
    os.system('service mongodb start')
    time.sleep(3)
    connection = pymongo.Connection() # try connect again

db = connection['lookupTables'] # create database

# clean up mongoDB collections
db.PID2LV.drop()
db.LV2MID.drop()
db.PID2LV
db.LV2MID

DELIMITER = '*'

LVNum = 4
MechineNum = 4
countRead = {}
countWrite = {}
sumWrite = 0
writeEnabledLVs = set( [ i for i in range(1, LVNum+1) ] )
readOnlyLVs = set()

connection = pymongo.Connection() # connect on the default host and port
db = connection['lookupTables'] # create database

def doRead( PID ):
    doc = db.PID2LV.find_one( { "PID" : PID } )
    if doc is not None:
        LV = doc["LV"]
        docs = db.LV2MID.find( { "LV" : LV } )
        MIDs = []
        for doc in docs:
           MIDs.append( doc["MID"] )
        MID = 0
        count = ( 1 << 31 ) -1
        for mid in MIDs:
           cnt = countRead[ mid ]
           if cnt < count:
               MID = mid
               count = cnt
        countRead[ MID ] += 1
        return ( LV, MID+1 )
    else:
        return ( -1, -1 )

def calVariance( MIDs ):
    global sumWrite
    mean = sumWrite + len( MIDs )   
    mean /= len( countWrite )
    var = 0
    for m in countWrite:
        if m in MIDs:
            var += ( countWrite[ m ] + 1 - mean ) ** 2
        else:
            var += ( countWrite[ m ] - mean ) ** 2
    return var / len( countWrite )

def doWrite( PID ):
    #TODO add disk capacity limitations
    global sumWrite
    LV = 0
    count = ( 1 << 31 ) - 1 
    for lv in writeEnabledLVs:
        docs = db.LV2MID.find( { "LV" : lv } )
        MIDs = []
        for doc in docs:
           MIDs.append( doc["MID"] )
        variance = calVariance( MIDs )    
        if variance < count:
            LV = lv
            count = variance
    print "Insert PID ", PID, " LV ", LV
    db.PID2LV.insert( { "PID" : PID, "LV" : LV } )
    docs = db.LV2MID.find( { "LV" : LV } )
    MIDs = []
    for doc in docs:
       m = doc["MID"]
       countWrite[ m ] += 1
       sumWrite += 1
       MIDs.append( str( m ) )
    return LV, MIDs

def doDelete( PID ):
    #TODO add disk capacity limitations
    global sumWrite
    LVobj = db.PID2LV.find_one( {"PID" : PID} )
    try:
        LV = LVobj['LV']
    except:
        print 'Can not find the LV of the PID, must have been deleted!' 
        return -1, ['-1'] # already deleted

    print "Delete: PID ", PID, " LV ", LV
    db.PID2LV.remove( { "PID" : PID } )
    docs = db.LV2MID.find( { "LV" : LV } )
    MIDs = []
    for doc in docs:
       m = doc["MID"]
       MIDs.append( str( m ) )
    return LV, MIDs

def updateReadOnly( LV ):
    writeEnabledLVs.remove( LV )
    readOnlyLVs.add( LV )

def updateWriteEnable( LV ):
    readOnlyLVs.remove( LV )
    writeEnabledLVs.add( LV ) 

# ################# Handler #################
class Handler(BaseHTTPRequestHandler):
    def do_GET(self):
        print 'GET received: ' + self.path
        self.send_response(200)
        self.end_headers()
        urlInfo = self.extractGetInfo(self.path) # get LV(MID) information
        action = int(urlInfo[0]) 
        ret = ''
        if action == ACTION_READ:
            pid = int(urlInfo[1])
            LV, MID = doRead( pid )
            ret = str(LV) + DELIMITER + str(MID)
            #print 'Here R blah'


        elif action == ACTION_WRITE:
            pid = int(urlInfo[1])
            LV,MIDs = doWrite( pid ) # assign LV and MIDs for each photo uploaded
            ret = str(LV) + DELIMITER + DELIMITER.join( MIDs )
            #print 'Here W LOL'

        elif action == ACTION_DELETE:
            pid = int(urlInfo[1])
            LV, MIDs = doDelete( pid )
            ret = str(LV) + DELIMITER + DELIMITER.join( MIDs )
            print 'After doDelete, ret: ' + ret

        elif action == ACTION_READONLY:
            LV = int(urlInfo[1])
            updateReadOnly( LV )

        elif action == ACTION_WRITEENABLE:
            LV = int(urlInfo[1])
            updateWriteEnable( LV )

        self.wfile.write( ret ) # send LV(MID) back to requester
        
        print "Count Write:"
        for m in countWrite:
            print m, countWrite[ m ]
        print "Count Read:"
        for m in countRead:
            print m, countRead[ m ]
        return

    def do_POST(self):
        pass
        # print 'POST received: ' + self.path
        # length = int(self.headers.getheader('content-length'))
        # data = self.rfile.read(length)
        # self.send_response(200)
        # self.end_headers()

        # urlInfo = self.extractPostInfo(self.path)
        # photoID = int( urlInfo[0] )
        # LV = int( urlInfo[1] )
        # savePhoto(photoID, LV, data)
        # return

    # return [photoID, LV]
    def extractPostInfo(self, requestPath): #  /12*1
        pass
        # requestPath = requestPath.split('/')[1]
        # urlInfo = requestPath.split(DELIMITER)
        # logging.debug('[photoID, LV]: ' + str(urlInfo))
        # pid = urlInfo[0]
        # LV = urlInfo[1]
        # return [pid, LV]

    # return [photoID, action]
    def  extractGetInfo(self, requestPath): #   /12*L1*P1
        requestPath = requestPath.split('/')[1]
        urlInfo = requestPath.split(DELIMITER)
        logging.debug('[action, PID/LV]: ' + str(urlInfo))
        action = urlInfo[0]
        pid_lv = urlInfo[1]    

        return [action, pid_lv]


class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    """Handle requests in a separate thread."""


def main():
    populateTables(LVNum, MechineNum)
    try:
        server = ThreadedHTTPServer(("", DIREC_PORT), Handler)
        print 'Starting server, use <Ctrl-C> to stop'
        print 'Directory Listening on port ' + str(DIREC_PORT) + '...'
        server.serve_forever()

    except KeyboardInterrupt:
        print '<Ctrl-C> received, shutting down the web server'
        server.socket.close()
        cleanUp()  # delete all saved physical volume files
        logging.debug('Store server terminated')

# set debug level
def setLogging(argv):
    if len(sys.argv) is 1:
        debugLevel = logging.INFO
    elif sys.argv[1] == 'debug':
        debugLevel = logging.DEBUG
    else:
        debugLevel = logging.INFO

    logging.basicConfig(level=debugLevel, format='%(message)s')

# delete lookup tables
def cleanUp():
    db.PID2LV.drop()
    db.LV2MID.drop()

def populateTables(logicalVolumNum, mechineNum):
    for i in range( logicalVolumNum ):
        db.LV2MID.insert( {"LV" : i, "MID": i } )
        db.LV2MID.insert( {"LV" : i, "MID": ( i + 1 ) % MechineNum } )
    for i in range( mechineNum ):
        countWrite[ i ] = 0
        countRead[ i ] = 0

if  __name__ =='__main__':
    setLogging(sys.argv)
    main()
